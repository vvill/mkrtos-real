#pragma once

void irq_test(void);
void rpc_test(void);
void fs_test(void);
void ns_test(void);
void malloc_test(void);
void mr_drv_test(void);
void pm_test(void);
